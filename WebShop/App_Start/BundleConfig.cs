﻿using System.Web;
using System.Web.Optimization;

namespace WebShop
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/base.js"));
                          
                        

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/gridmvc").Include(
                       "~/Scripts/gridmvc.js*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui-datepicker-fr").Include(
                   "~/Scripts/jquery.ui.datepicker-fr.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                   "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
                    "~/Content/themes/base/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/DataTables/css/jquery.dataTables.css",
                      "~/Content/bootstrap-lumen.css",
                      "~/Content/Gridmvc.css",
                      "~/Content/site.css",
                       "~/Content/themes/base/all.css",
                       "~/Content/themes/base/jquery-ui.css"));

            bundles.Add(new ScriptBundle("~/bundles/DataTables").Include(
      "~/Scripts/DataTables-1.10.7/media/js/jquery.dataTables.min.js",
      "~/Scripts/DataTables-1.10.7/media/js/dataTables.bootstrap.js",
      "~/Scripts/DataTables/jquery.dataTables.js",
      "~/Scripts/DataTables-1.10.7/media/js/dataTables.paging.bootstrap.js"));

            //bundles.Add(new ScriptBundle("~/bundles/DataTables").Include(
            //           "~/Scripts/DataTables/jquery.dataTables.js"
            //           ));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css",
            //          "~/Content/Datatables/css/jquery.dataTables.css"));
        }
    }
}
