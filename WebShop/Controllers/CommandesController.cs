﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.Models;
using WebShop.CustomObjects;
using WebShop.BusinessLayer;

namespace WebShop.Controllers
{
    public class CommandesController : Controller
    {
        private WebShopEntities db = new WebShopEntities();

        [HttpGet]
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                var list = BLCommandes.GetAllData();
                return View(list);

            }
            else
            {
                ClientsModel cli = BLClients.FindClientByName(User.Identity.Name);
                var lstCommande = BLCommandes.GetAllData().Where(c => c.COM_CLI_Id == cli.CLI_Id);
                return View(lstCommande);
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            LoadBasicViewBagCiviliteClient();
            LoadBasicViewBagEtatCommandeCreate();
            var client = BLClients.GetAllData();
            SelectList list = new SelectList(client, "CLI_Id", "CLI_Nom");
            ViewBag.Commande = list;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommandesModel Commande)
        {
            if (ModelState.IsValid)
            {
                BLCommandes.AjouterCommande(Commande, User.Identity.Name);
                return RedirectToAction("Index");
            }
            var client = BLClients.GetAllData();
            SelectList list = new SelectList(client, "CLI_Id", "CLI_Nom");
            LoadBasicViewBagCiviliteClient();
            LoadBasicViewBagEtatCommandeCreate();
            ViewBag.Commande = list;
            return View(Commande);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LoadBasicViewBagCiviliteClient();
            LoadBasicViewBagEtatCommande();
            var commande = BLCommandes.FindCommandeById(id);

            if (commande == null)
            {
                return HttpNotFound();
            }

            ViewBag.Commande = new SelectList(db.Client, "CLI_Id", "CLI_Nom", commande.COM_CLI_Id);

            return View(commande);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommandesModel Commande)
        {
            if (ModelState.IsValid)
            {
                BLCommandes.Edit(Commande);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            LoadBasicViewBagCiviliteClient();
            LoadBasicViewBagEtatCommande();
            ViewBag.COM_CLI_Id = new SelectList(db.Client, "CLI_Id", "CLI_Nom", Commande.COM_CLI_Id);
            return View(Commande);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var commande = BLCommandes.FindCommandeById(id);
            if (commande == null)
            {
                return HttpNotFound();
            }
            return View(commande);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BLCommandes.DeleteById(id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void LoadBasicViewBagEtatCommande()
        {
            var lstEtat = new List<Etat>
            {
                new Etat{ID = "En Attente", Choice = "En attente"},
                new Etat{ID = "Livraison en cours", Choice = "Livraison en cours"},
                new Etat {ID = "Livraison terminée", Choice = "Livraison terminée"},
            };
            ViewBag.ListEtat = new SelectList(lstEtat, "ID", "Choice");
        }
        private void LoadBasicViewBagCiviliteClient()
        {
            var lstEtat = new List<Civilite>
            {
                //new Civilite{ID = "---", Choice = "---"},
                new Civilite{ID = "Monsieur", Choice = "Monsieur"},
                new Civilite {ID = "Madame", Choice = "Madame"}
            };
            ViewBag.ListCivilite = new SelectList(lstEtat, "ID", "Choice");
        }
        private void LoadBasicViewBagEtatCommandeCreate()
        {
            var lstEtat = new List<Etat>
            {
                new Etat{ID = "En Attente", Choice = "En attente"}
                
            };
            ViewBag.ListEtatCommandeCreate = new SelectList(lstEtat, "ID", "Choice");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
