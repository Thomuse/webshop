﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.CustomObjects;
using WebShop.Models;
using WebShop.BusinessLayer;

namespace WebShop.Controllers
{
    public class ArticlesController : Controller
    {
        private WebShopEntities db = new WebShopEntities();

        [HttpGet]
        public ActionResult Index()
        {
            var categorie = BLCategories.GetAllData();
            SelectList list = new SelectList(categorie,"CAT_ID","CAT_Libelle");
            ViewBag.Categorie = list;
            var souscategorie = BLSousCategories.GetAllData();
            SelectList liste = new SelectList(souscategorie, "SCAT_CAT_Id", "SCAT_Libelle");
            ViewBag.SousCategorie = liste;
            List<ArticlesModel> lstArticle =BLArticles.GetAllData();

            var panier = new Panier();
            var art = new Article();

            if (panier.ArticleId > 0)
            {
                int resultat;
                resultat = art.ART_Stock - panier.ArticleId;
            }
            return View(lstArticle);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Article.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.ART_SCAT_Id = new SelectList(db.SousCategorie, "SCAT_Id", "SCAT_Libelle");
            //return View();
            return PartialView("_Create");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(ArticlesModel Article)
        {
            if (ModelState.IsValid)
            {
                BLArticles.AjouterArticle(Article);
                //db.SaveChanges();
                //return RedirectToAction("Index");
                return Json(new { result = "ok" });
            }

            ViewBag.ART_SCAT_Id = new SelectList(db.SousCategorie, "SCAT_Id", "SCAT_Libelle", Article.ART_SCAT_Id);
            return View(Article);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var article = BLArticles.FindArticleById(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            ViewBag.ART_SCAT_Id = new SelectList(db.SousCategorie, "SCAT_Id", "SCAT_Libelle", article.ART_SCAT_Id);
            return View(article);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ArticlesModel Article)
        {
            if (ModelState.IsValid)
            {
                var article = BLArticles.Edit(Article);
                return RedirectToAction("Index");
            }
            ViewBag.ART_SCAT_Id = new SelectList(db.SousCategorie, "SCAT_Id", "SCAT_Libelle", Article.ART_SCAT_Id);
            return View(Article);

        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var article = BLArticles.FindArticleById(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BLArticles.DeleteById(id);

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult GetCategorie(int catid)
        {
            var art = BLArticles.GetAllData().ToList();
            var sousCat = BLSousCategories.GetAllData().FindAll(x => x.SCAT_CAT_Id == catid).ToList();
            var articles = new List<ArticlesModel>();

            foreach (var sc in sousCat)
            {
                foreach (var a in art)
                {
                    if (a.SousCategorie.SCAT_Id == sc.SCAT_Id)
                    {
                        articles.Add(a);
                    }
                }
            }

            return Json(new { souscat = sousCat, articles = articles }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
