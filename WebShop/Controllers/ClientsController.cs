﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.CustomObjects;
using WebShop.Models;
using WebShop.BusinessLayer;

namespace WebShop.Controllers
{
    public class ClientsController : Controller
    {
        private WebShopEntities db = new WebShopEntities();

      [HttpGet]
        public ActionResult Index()
        {
            List<ClientsModel> lstClient = BLClients.GetAllData();
            return View(lstClient);
        }

        [HttpGet]
        public ActionResult Create()
        {
            LoadBasicViewBag();
            return View(new ClientsModel());
            
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClientsModel client)
        {
            if (ModelState.IsValid)
            {
               BLClients.AjouterClient(client);
                return RedirectToAction("Index");
            }

            return View(client);
        }

        private void LoadBasicViewBag()
        {
            var lstEtat = new List<Civilite>
            {
                //new Civilite{ID = "--", Choice = "---"},
                new Civilite{ID = "Monsieur", Choice = "Monsieur"},
                new Civilite {ID = "Madame", Choice = "Madame"}
            };
            ViewBag.ListCivilite = new SelectList(lstEtat, "ID", "Choice");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = BLClients.FindClientById(id);
            LoadBasicViewBag();
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit (ClientsModel Client)
        {
            if (ModelState.IsValid)
            {
                var client = BLClients.Edit(Client);
                return RedirectToAction("Index");
            }
            return View(Client);
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            var client = BLClients.FindClientById(id);
            if (client == null)
            {

                return HttpNotFound();
            }
            return View(client);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            

                BLClients.DeleteClientById(id);
                return RedirectToAction("Index");
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
