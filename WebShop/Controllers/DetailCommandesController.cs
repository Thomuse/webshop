﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.BusinessLayer;

namespace WebShop.Models
{
    public class DetailCommandesController : Controller
    {
        private WebShopEntities db = new WebShopEntities();

        [HttpGet]
        public ActionResult Index()
        {
            List<DetailsCommandeModel> lstDetailCommande = BLDetailsCommandes.GetAllData();
            return View(lstDetailCommande);
        }
      
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.DCOM_ART_Id = new SelectList(db.Article, "ART_Id", "ART_Libelle");
            ViewBag.DCOM_COM_Id = new SelectList(db.Commande, "COM_Id", "COM_Statut");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
         public ActionResult Create (DetailsCommandeModel DetailsCommande)
        {
            if (ModelState.IsValid)
            {
                BLDetailsCommandes.AjouterDetailsCommande(DetailsCommande);
                return RedirectToAction("Index");
            }

            ViewBag.DCOM_ART_Id = new SelectList(db.Article, "ART_Id", "ART_Libelle", DetailsCommande.DCOM_ART_Id);
            ViewBag.DCOM_COM_Id = new SelectList(db.Commande, "COM_Id", "COM_Statut", DetailsCommande.DCOM_COM_Id);
            return View(DetailsCommande);
        }
         [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var detailCommande = BLDetailsCommandes.FindByCommandeId(id);
            if (detailCommande == null)
            {
                return HttpNotFound();
            }
            ViewBag.DCOM_ART_Id = new SelectList(db.Article, "ART_Id", "ART_Libelle", detailCommande.DCOM_ART_Id);
            ViewBag.DCOM_COM_Id = new SelectList(db.Commande, "COM_Id", "COM_Statut", detailCommande.DCOM_COM_Id);
            return View(detailCommande);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DetailsCommandeModel detailCommande)
        {
            if (ModelState.IsValid)
            {
                BLDetailsCommandes.Edit(detailCommande);
                return RedirectToAction("Index");
            }
            ViewBag.DCOM_ART_Id = new SelectList(db.Article, "ART_Id", "ART_Libelle", detailCommande.DCOM_ART_Id);
            ViewBag.DCOM_COM_Id = new SelectList(db.Commande, "COM_Id", "COM_Statut", detailCommande.DCOM_COM_Id);
            return View(detailCommande);
        }
         [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var detailCommande = BLDetailsCommandes.FindByCommandeId(id);
            if (detailCommande == null)
            {
                return HttpNotFound();
            }
            return View(detailCommande);
        }
         
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           BLDetailsCommandes.DeleteDetailsCommande(id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
