﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.Models;
using WebShop.BusinessLayer;

namespace WebShop.Controllers
{
    public class SousCategoriesController : Controller
    {
        private WebShopEntities db = new WebShopEntities();

        [HttpGet]
        public ActionResult Index()
        {
            List<SousCategoriesModel> lstSousCategorie = BLSousCategories.GetAllData();
            return View(lstSousCategorie);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.SCAT_CAT_Id = new SelectList(db.Categorie, "CAT_Id", "CAT_Libelle");
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SousCategoriesModel SousCategorie)
        {
            if (ModelState.IsValid)
            {
               BLSousCategories.AjouterSousCategorie(SousCategorie);
                return RedirectToAction("Index");
            }

            ViewBag.SCAT_CAT_Id = new SelectList(db.Categorie, "CAT_Id", "CAT_Libelle", SousCategorie.SCAT_CAT_Id);
            return View(SousCategorie);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sousCategorie = BLSousCategories.FindSousCategorieById(id);
            if (sousCategorie == null)
            {
                return HttpNotFound();
            }
            ViewBag.SCAT_CAT_Id = new SelectList(db.Categorie, "CAT_Id", "CAT_Libelle", sousCategorie.SCAT_CAT_Id);
            return View(sousCategorie);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SousCategoriesModel SousCategorie)
        {
            if (ModelState.IsValid)
            {
                var sousCategorie = BLSousCategories.Edit(SousCategorie);
                return RedirectToAction("Index");
            }
            ViewBag.SCAT_CAT_Id = new SelectList(db.Categorie, "CAT_Id", "CAT_Libelle", SousCategorie.SCAT_CAT_Id);
            return View(SousCategorie);
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var SousCategorie = BLSousCategories.FindSousCategorieById(id);
            if (SousCategorie == null)
            {
                return HttpNotFound();
            }
            return View(SousCategorie);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BLSousCategories.DeleteSousCategorieById(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
