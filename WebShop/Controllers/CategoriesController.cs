﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.Models;
using WebShop.BusinessLayer;

namespace WebShop.Controllers
{
    public class CategoriesController : Controller
    {
        private WebShopEntities db = new WebShopEntities();

        [HttpGet]
        public ActionResult Index()
        {
            List<CategoriesModel> ListCategorie = BLCategories.GetAllData();
            return View(ListCategorie);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var Categorie = BLCategories.GetAllData();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoriesModel Categorie)
        {
            if (ModelState.IsValid)
            {
                BLCategories.AjouterCategorie(Categorie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var categorie = BLCategories.GetAllData();

            return View(Categorie);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var categorie = BLCategories.FindCategorieById(id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoriesModel Categorie)
        {
            if (ModelState.IsValid)
            {
                var categorie = BLCategories.Edit(Categorie);
                return RedirectToAction("Index");
            }
            return View(Categorie);
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var categorie = BLCategories.FindCategorieById(id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BLCategories.DeleteById(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
