﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.DAL;
using WebShop.BusinessLayer;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class PanierController : Controller
    {
        private WebShopEntities dbContext = new WebShopEntities();

            [HttpGet]
        public ActionResult Index()
        {
            var lst = (List<Article>)Session["Articles"];

            if (lst != null)
            {
                var article = lst.GroupBy(x => x.ART_Libelle).ToList()
                    .Select(x => new WebShop.DAL.Panier()
                    {
                        Article = new Article { ART_Libelle = x.Key, ART_Prix = BLArticles.GetByName(x.Key).ART_Prix, ART_Id = BLArticles.GetByName(x.Key).ART_Id },
                        Count = x.Count(),
                    }).ToList();

                var viewModel = new PanierModel
                {
                    CartItems = article,
                    CartTotal = ToAdd(article)
                };

            return View(viewModel);
            }
            return RedirectToAction("Index", "Articles");
        }
        private decimal ToAdd(List<DAL.Panier> lst)
        {
            decimal total = 0;

            foreach (var item in lst)
            {
                total += item.Article.ART_Prix * item.Count;
            }
            return total;
        }

        [HttpGet]
        public ActionResult AddToCart(int id)
        {
            var addedAlbum = dbContext.Article
                .Single(art => art.ART_Id == id);

            var lst = (List<Article>)Session["Articles"];
            if(lst!=null)
                lst.Add(addedAlbum);
            BLArticles.AddToCartDecreaseDB(id);
                Session["Articles"] = lst;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult RemoveFromCart(int id)
        {

            var article = dbContext.Article
                .Single(art => art.ART_Id == id);

            var lst = (List<Article>)Session["Articles"];
            if (lst.Count >= 1)
            {
            int index = lst.FindIndex(art => art.ART_Id == id);
                lst.RemoveAt(index);
            }
            BLArticles.RemoveToCartUpdateDB(id);
            Session["Articles"] = lst;


            var results = new ViderPanierModel
            {
                Message = Server.HtmlEncode(article.ART_Libelle) + " has been removed from your shopping cart.",
                
            };



            return Json(results);
        }
        }
    }
