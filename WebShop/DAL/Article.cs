namespace WebShop.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Article")]
    public partial class Article
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Article()
        {
            DetailCommande = new HashSet<DetailCommande>();
            Panier = new HashSet<Panier>();
        }

        [Key]
        public int ART_Id { get; set; }

        public int ART_SCAT_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ART_Libelle { get; set; }

        [Required]
        public string ART_Description { get; set; }

        public decimal ART_Prix { get; set; }

        public int ART_Stock { get; set; }

        public string Image { get; set; }

        public virtual SousCategorie SousCategorie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailCommande> DetailCommande { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Panier> Panier { get; set; }
    }
}
