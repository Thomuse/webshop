namespace WebShop.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WebShopEntities : DbContext
    {
        public WebShopEntities()
            : base("name=WebShopEntities2")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Categorie> Categorie { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Commande> Commande { get; set; }
        public virtual DbSet<DetailCommande> DetailCommande { get; set; }
        public virtual DbSet<Panier> Panier { get; set; }
        public virtual DbSet<SousCategorie> SousCategorie { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>()
                .Property(e => e.ART_Libelle)
                .IsUnicode(false);

            modelBuilder.Entity<Article>()
                .Property(e => e.ART_Description)
                .IsUnicode(false);

            modelBuilder.Entity<Article>()
                .Property(e => e.ART_Prix)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Article>()
                .HasMany(e => e.DetailCommande)
                .WithRequired(e => e.Article)
                .HasForeignKey(e => e.DCOM_ART_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Article>()
                .HasMany(e => e.Panier)
                .WithRequired(e => e.Article)
                .HasForeignKey(e => e.ArticleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.Client)
                .WithOptional(e => e.AspNetUsers)
                .HasForeignKey(e => e.CLI_AspUser_Id);

            modelBuilder.Entity<Categorie>()
                .Property(e => e.CAT_Libelle)
                .IsUnicode(false);

            modelBuilder.Entity<Categorie>()
                .HasMany(e => e.SousCategorie)
                .WithRequired(e => e.Categorie)
                .HasForeignKey(e => e.SCAT_CAT_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Prenom)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Civilite)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Email)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Adresse)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_CodePostal)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Ville)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CLI_Telephone)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Commande)
                .WithRequired(e => e.Client)
                .HasForeignKey(e => e.COM_CLI_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Statut)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Prenom)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Civilite)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Adresse)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_CodePostal)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Ville)
                .IsUnicode(false);

            modelBuilder.Entity<Commande>()
                .Property(e => e.COM_Total)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Commande>()
                .HasMany(e => e.DetailCommande)
                .WithRequired(e => e.Commande)
                .HasForeignKey(e => e.DCOM_COM_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DetailCommande>()
                .Property(e => e.DCOM_PrixUnitaire)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Panier>()
                .Property(e => e.CartId)
                .IsUnicode(false);

            modelBuilder.Entity<SousCategorie>()
                .Property(e => e.SCAT_Libelle)
                .IsUnicode(false);

            modelBuilder.Entity<SousCategorie>()
                .HasMany(e => e.Article)
                .WithRequired(e => e.SousCategorie)
                .HasForeignKey(e => e.ART_SCAT_Id)
                .WillCascadeOnDelete(false);
        }
    }
}
