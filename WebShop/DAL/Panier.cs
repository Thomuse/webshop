namespace WebShop.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Panier")]
    public partial class Panier
    {
        public int PanierId { get; set; }

        [Required]
        [StringLength(50)]
        public string CartId { get; set; }

        public int ArticleId { get; set; }

        public int Count { get; set; }

        public DateTime? DateCreate { get; set; }

        public virtual Article Article { get; set; }
    }
}
