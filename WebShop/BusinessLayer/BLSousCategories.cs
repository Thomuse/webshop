﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebShop.DAL;
using WebShop.Models;

namespace WebShop.BusinessLayer
{
    public static class BLSousCategories
    {
        public static List<SousCategoriesModel> GetAllData()
        {
            List<SousCategoriesModel> ListSousCategorie = new List<SousCategoriesModel>();

            using (WebShopEntities ContextEf = new WebShopEntities())
            {
                foreach (var item in ContextEf.SousCategorie)
                {
                    var cat = BLCategories.GetAllData().SingleOrDefault(x => x.CAT_Id == item.SCAT_CAT_Id);
                    ListSousCategorie.Add(new SousCategoriesModel
                    {

                        //Article = item.Article,
                        //Categorie = item.Categorie,
                        SCAT_CAT_Id = item.SCAT_CAT_Id,
                        SCAT_Id = item.SCAT_Id,
                        SCAT_Libelle = item.SCAT_Libelle,
                        Categorie = cat
                    });
                }
                return ListSousCategorie;


            }

        }
        public static void AjouterSousCategorie(SousCategoriesModel SousCategorie)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                dbContext.SousCategorie.Add(new SousCategorie
                {
                    //Article = SousCategorie.Article,
                    //Categorie = SousCategorie.Categorie,
                    SCAT_CAT_Id = SousCategorie.SCAT_CAT_Id,
                    SCAT_Id = SousCategorie.SCAT_Id,
                    SCAT_Libelle = SousCategorie.SCAT_Libelle

                });
                dbContext.SaveChanges();
            }

        }
        public static SousCategoriesModel FindSousCategorieById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                SousCategorie SousCategorie = dbContext.SousCategorie.Find(id);
                SousCategoriesModel souscategorie = new SousCategoriesModel
                {
                    //Article = SousCategorie.Article,
                    //Categorie = SousCategorie.Categorie,
                    SCAT_CAT_Id = SousCategorie.SCAT_CAT_Id,
                    SCAT_Id = SousCategorie.SCAT_Id,
                    SCAT_Libelle = SousCategorie.SCAT_Libelle
                };
                return souscategorie;
            }

        }
        public static void DeleteSousCategorieById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                SousCategorie souscategorie = dbContext.SousCategorie.Find(id);
                dbContext.SousCategorie.Remove(souscategorie);
                dbContext.SaveChanges();

            }

        }
        public static SousCategoriesModel Edit(SousCategoriesModel SousCategorie)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                SousCategorie souscategorie = new SousCategorie
                {
                    //Article = SousCategorie.Article,
                    //Categorie = SousCategorie.Categorie,
                    SCAT_CAT_Id = SousCategorie.SCAT_CAT_Id,
                    SCAT_Id = SousCategorie.SCAT_Id,
                    SCAT_Libelle = SousCategorie.SCAT_Libelle
                };
                dbContext.Entry(souscategorie).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                return SousCategorie;
            }

        }
    }
}