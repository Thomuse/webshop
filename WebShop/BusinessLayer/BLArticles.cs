﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebShop.DAL;
using WebShop.Models;

namespace WebShop.BusinessLayer
{
    public static class BLArticles
    {
        public static List<ArticlesModel> GetAllData()
        {
            List<ArticlesModel> lstArticles = new List<ArticlesModel>();
            using (DAL.WebShopEntities ContextEf = new DAL.WebShopEntities())
            {
                foreach (var item in ContextEf.Article)
                {
                    var SousCat = BLSousCategories.GetAllData().SingleOrDefault(x => x.SCAT_Id == item.ART_SCAT_Id);

                    lstArticles.Add(new ArticlesModel
                    {
                        ART_Id = item.ART_Id,
                        ART_Libelle = item.ART_Libelle,
                        ART_Description = item.ART_Description,
                        ART_Prix = item.ART_Prix,
                        ART_SCAT_Id = item.ART_SCAT_Id,
                        ART_Stock = item.ART_Stock,
                        SousCategorie = SousCat,
                        Image = item.Image

                        

                    });
                }
                return lstArticles;
            }
        }

        public static void AjouterArticle(ArticlesModel Article)
        {
            using (DAL.WebShopEntities dbContext = new DAL.WebShopEntities())
            {
                dbContext.Article.Add(new Article
                {
                    ART_Description = Article.ART_Description,
                    ART_Id = Article.ART_Id,
                    ART_Libelle = Article.ART_Libelle,
                    ART_Prix = Article.ART_Prix,
                    ART_SCAT_Id = Article.ART_SCAT_Id,
                    ART_Stock = Article.ART_Stock,
                    Image = Article.Image

                });
                dbContext.SaveChanges();
            }

        }
        public static ArticlesModel FindArticleById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Article Article = dbContext.Article.Find(id);
                var SousCat = BLSousCategories.GetAllData().SingleOrDefault(x => x.SCAT_Id == Article.ART_SCAT_Id);

                ArticlesModel article = (new ArticlesModel
                {
                    ART_Description = Article.ART_Description,
                    ART_Id = Article.ART_Id,
                    ART_Libelle = Article.ART_Libelle,
                    ART_Prix = Article.ART_Prix,
                    ART_SCAT_Id = Article.ART_SCAT_Id,
                    ART_Stock = Article.ART_Stock,
                    SousCategorie = SousCat,
                    Image = Article.Image                   
                });

                return article;
            }
        }
        public static void DeleteById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Article Article = dbContext.Article.Find(id);
                dbContext.Article.Remove(Article);
                dbContext.SaveChanges();
            }
        }
        public static ArticlesModel Edit(ArticlesModel article)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {

                Article Article = new Article
                {
                    ART_Description = article.ART_Description,
                    ART_Id = article.ART_Id,
                    ART_Libelle = article.ART_Libelle,
                    ART_Prix = article.ART_Prix,
                    ART_SCAT_Id = article.ART_SCAT_Id,
                    ART_Stock = article.ART_Stock,
                    Image = article.Image

                };
                dbContext.Entry(Article).State = System.Data.Entity.EntityState.Modified;

                dbContext.SaveChanges();
                return article;

            }
        }
    
             public static ArticlesModel GetByName(string dd)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Article art = dbContext.Article.Where(x=>x.ART_Libelle==dd).FirstOrDefault();
                var SousCat = BLSousCategories.GetAllData().SingleOrDefault(x => x.SCAT_Id == art.ART_SCAT_Id);
                ArticlesModel ArticleModel = (new ArticlesModel
                {
                    ART_Id = art.ART_Id,
                    ART_Description = art.ART_Description,
                    ART_Libelle = art.ART_Libelle,
                    ART_Prix = art.ART_Prix,
                    ART_SCAT_Id = art.ART_SCAT_Id,
                    ART_Stock = art.ART_Stock,
                    SousCategorie = SousCat,
                    Image = art.Image
                });
                return ArticleModel;
            }
        }
             public static void AddToCartDecreaseDB(int id)
             {
                 using (WebShopEntities dbContext = new WebShopEntities())
                 {
                     var lst = dbContext.Article.Where(x => x.ART_Id == id).FirstOrDefault();
                     lst.ART_Stock = lst.ART_Stock - 1; 
                     dbContext.SaveChanges();
                 }
             }

             public static void RemoveToCartUpdateDB(int id)
             {
                 using (WebShopEntities dbContext = new WebShopEntities())
                 {
                     var lst = dbContext.Article.Where(x => x.ART_Id == id).FirstOrDefault();
                     lst.ART_Stock = lst.ART_Stock + 1;
                     dbContext.SaveChanges();
                 }
             }

             public static void ClearCart(List<Article> lst)
             {
                 using (WebShopEntities dbContext = new WebShopEntities())
                 {
                     foreach (var item in lst)
                     {
                         var article = dbContext.Article.Where(x => x.ART_Id == item.ART_Id).FirstOrDefault();
                         article.ART_Stock += 1;


                     }
                     dbContext.SaveChanges();
                 }
             }



        }
    }
