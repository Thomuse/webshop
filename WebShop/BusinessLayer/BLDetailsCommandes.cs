﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebShop.Models;
using WebShop.DAL;

namespace WebShop.BusinessLayer
{
    public static class BLDetailsCommandes
    {
        public static List<DetailsCommandeModel> GetAllData()
        {
            List<DetailsCommandeModel> listDetailsCommande = new List<DetailsCommandeModel>();
            var DetailCommande = new DetailCommande();
            using (WebShopEntities ContextEf = new WebShopEntities())
            {
                foreach (var item in ContextEf.DetailCommande)
                {
                    listDetailsCommande.Add(new DetailsCommandeModel
                    {
                        DCOM_Id = DetailCommande.DCOM_Id,
                        DCOM_COM_Id = DetailCommande.DCOM_COM_Id,
                        DCOM_ART_Id = DetailCommande.DCOM_ART_Id,
                        DCOM_Quantite = DetailCommande.DCOM_Quantite,
                        DCOM_PrixUnitaire = DetailCommande.DCOM_PrixUnitaire,
                        //Article = DetailCommande.Article,
                        //Commande = DetailCommande.Commande
                    });
                }
                return listDetailsCommande;
            }


        }
        public static void AjouterDetailsCommande(DetailsCommandeModel DetailsCommande)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                dbContext.DetailCommande.Add(new DetailCommande
                {
                    DCOM_ART_Id = DetailsCommande.DCOM_ART_Id,
                    DCOM_COM_Id = DetailsCommande.DCOM_COM_Id,
                    DCOM_Id = DetailsCommande.DCOM_Id,
                    DCOM_Quantite = DetailsCommande.DCOM_Quantite,
                    DCOM_PrixUnitaire = DetailsCommande.DCOM_PrixUnitaire,
                    //Article = DetailsCommande.Article,
                    //Commande = DetailsCommande.Commande
                });

                dbContext.SaveChanges();
            }


        }
        public static DetailsCommandeModel FindByCommandeId(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                DetailCommande DetailCommande = dbContext.DetailCommande.Find(id);
                DetailsCommandeModel detailsCommande = (new DetailsCommandeModel
                {
                    DCOM_Id = DetailCommande.DCOM_Id,
                    DCOM_COM_Id = DetailCommande.DCOM_COM_Id,
                    DCOM_ART_Id = DetailCommande.DCOM_ART_Id,
                    DCOM_Quantite = DetailCommande.DCOM_Quantite,
                    DCOM_PrixUnitaire = DetailCommande.DCOM_PrixUnitaire,
                    //Article = DetailCommande.Article,
                    //Commande = DetailCommande.Commande

                });
                return detailsCommande;
            }
        }
        public static void DeleteDetailsCommande(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                DetailCommande DetailCommande = dbContext.DetailCommande.Find(id);
                dbContext.DetailCommande.Remove(DetailCommande);
                dbContext.SaveChanges();
            }
        }
        public static DetailsCommandeModel Edit(DetailsCommandeModel DetailsCommande)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                DetailCommande detailscommande = new DetailCommande
                {
                    DCOM_ART_Id = DetailsCommande.DCOM_ART_Id,
                    DCOM_COM_Id = DetailsCommande.DCOM_COM_Id,
                    DCOM_Id = DetailsCommande.DCOM_Id,
                    DCOM_PrixUnitaire = DetailsCommande.DCOM_PrixUnitaire,
                    DCOM_Quantite = DetailsCommande.DCOM_Quantite,
                    //Article = DetailsCommande.Article,
                    //Commande = DetailsCommande.Commande
                };
                dbContext.Entry(detailscommande).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                return DetailsCommande;
            }
        }
    }
}