﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebShop.DAL;
using WebShop.Models;

namespace WebShop.BusinessLayer
{
    public static class BLClients
    {
        public static List<ClientsModel> GetAllData()
        {
            List<ClientsModel> lstClient = new List<ClientsModel>();

            using (WebShopEntities ContextEf = new WebShopEntities())
            {

                foreach (var item in ContextEf.Client)
                {
                    lstClient.Add(new ClientsModel
                    {
                        CLI_Adresse = item.CLI_Adresse,
                        CLI_Civilite = item.CLI_Civilite,
                        CLI_CodePostal = item.CLI_CodePostal,
                        CLI_Email = item.CLI_Email,
                        CLI_Id = item.CLI_Id,
                        CLI_Nom = item.CLI_Nom,
                        CLI_Prenom = item.CLI_Prenom,
                        CLI_Telephone = item.CLI_Telephone,
                        CLI_Ville = item.CLI_Ville
                    });
                }
                return lstClient;




            }


        }



        public static void AjouterClient(ClientsModel clientModel)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                dbContext.Client.Add(new Client
                {


                    CLI_Adresse = clientModel.CLI_Adresse,
                    CLI_Civilite = clientModel.CLI_Civilite,
                    CLI_CodePostal = clientModel.CLI_CodePostal,
                    CLI_Email = clientModel.CLI_Email,
                    CLI_Id = clientModel.CLI_Id,
                    CLI_Nom = clientModel.CLI_Nom,
                    CLI_Prenom = clientModel.CLI_Prenom,
                    CLI_Telephone = clientModel.CLI_Telephone,
                    CLI_Ville = clientModel.CLI_Ville

                });
                dbContext.SaveChanges();
            }
        }
        public static ClientsModel FindClientById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Client Client = dbContext.Client.Find(id);
                ClientsModel client = new ClientsModel
                {
                    CLI_Adresse = Client.CLI_Adresse,
                    CLI_Civilite = Client.CLI_Civilite,
                    CLI_CodePostal = Client.CLI_CodePostal,
                    CLI_Email = Client.CLI_Email,
                    CLI_Id = Client.CLI_Id,
                    CLI_Nom = Client.CLI_Nom,
                    CLI_Prenom = Client.CLI_Prenom,
                    CLI_Telephone = Client.CLI_Telephone,
                    CLI_Ville = Client.CLI_Ville
                };

                return client;
            }
        }

        public static ClientsModel FindClientByName(string username)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Client Client = dbContext.Client.FirstOrDefault(c => c.CLI_Email == username);
                ClientsModel client = new ClientsModel
                {
                    CLI_Adresse = Client.CLI_Adresse,
                    CLI_Civilite = Client.CLI_Civilite,
                    CLI_CodePostal = Client.CLI_CodePostal,
                    CLI_Email = Client.CLI_Email,
                    CLI_Id = Client.CLI_Id,
                    CLI_Nom = Client.CLI_Nom,
                    CLI_Prenom = Client.CLI_Prenom,
                    CLI_Telephone = Client.CLI_Telephone,
                    CLI_Ville = Client.CLI_Ville
                };

                return client;
            }
        }
        public static void DeleteClientById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Client Client = dbContext.Client.Find(id);
                dbContext.Client.Remove(Client);
                dbContext.SaveChanges();


            }

        }
        public static ClientsModel Edit(ClientsModel Client)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Client client = new Client
                {
                    CLI_Adresse = Client.CLI_Adresse,
                    CLI_Civilite = Client.CLI_Civilite,
                    CLI_CodePostal = Client.CLI_CodePostal,
                    CLI_Email = Client.CLI_Email,
                    CLI_Id = Client.CLI_Id,
                    CLI_Nom = Client.CLI_Nom,
                    CLI_Prenom = Client.CLI_Prenom,
                    CLI_Telephone = Client.CLI_Telephone,
                    CLI_Ville = Client.CLI_Ville
                };
                dbContext.Entry(client).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                return Client;
            }

        }


    }
}