﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebShop.DAL;
using WebShop.Models;

namespace WebShop.BusinessLayer
{
    public static class BLCategories
    {
        public static List<CategoriesModel> GetAllData()
        {
            List<CategoriesModel> ListCategorie = new List<CategoriesModel>();
            var Categorie = new CategoriesModel();
            using (WebShopEntities ContextEf = new WebShopEntities())
            {
                foreach (var item in ContextEf.Categorie)
                {
                    ListCategorie.Add(new CategoriesModel
                    {
                        CAT_Id = item.CAT_Id,
                        CAT_Libelle = item.CAT_Libelle,
                        //SousCategorie = item.SousCategorie

                    });
                }
                return ListCategorie;
            }
        }
        public static void AjouterCategorie(CategoriesModel Categorie)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                dbContext.Categorie.Add(new Categorie
                {
                    CAT_Id = Categorie.CAT_Id,
                    CAT_Libelle = Categorie.CAT_Libelle,
                    //SousCategorie = Categorie.SousCategorie

                });
                dbContext.SaveChanges();
            }

        }
        public static CategoriesModel FindCategorieById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Categorie Categorie = dbContext.Categorie.Find(id);
                CategoriesModel categorie = new CategoriesModel
                {
                    CAT_Id = Categorie.CAT_Id,
                    CAT_Libelle = Categorie.CAT_Libelle
                };
                return categorie;
            }

        }
        public static void DeleteById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Categorie categorie = dbContext.Categorie.Find(id);
                dbContext.Categorie.Remove(categorie);
                dbContext.SaveChanges();
            }

        }
        public static CategoriesModel Edit(CategoriesModel categorie)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Categorie Categorie = new Categorie
                {
                    CAT_Id = categorie.CAT_Id,
                    CAT_Libelle = categorie.CAT_Libelle
                };
                dbContext.Entry(Categorie).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                return categorie;
            }

        }
    }
}