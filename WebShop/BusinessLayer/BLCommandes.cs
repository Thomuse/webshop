﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebShop.Models;
using WebShop.DAL;


namespace WebShop.BusinessLayer
{
    public static class BLCommandes
    {
        public static List<CommandesModel> GetAllData()
        {
            List<CommandesModel> ListCommande = new List<CommandesModel>();
            //var Commande = new CommandesModel();

            using (WebShopEntities ContextEf = new WebShopEntities())
            {
                foreach (var item in ContextEf.Commande)
                {
                    var cli = BLClients.GetAllData().SingleOrDefault(x => x.CLI_Id == item.COM_CLI_Id);
                    ListCommande.Add(new CommandesModel
                    {
                        COM_Adresse = item.COM_Adresse,
                        COM_Civilite = item.COM_Civilite,
                        COM_CLI_Id = item.COM_CLI_Id,
                        COM_CodePostal = item.COM_CodePostal,
                        COM_Date = item.COM_Date,
                        COM_DateLivraison = item.COM_DateLivraison,
                        COM_Id = item.COM_Id,
                        COM_Nom = item.COM_Nom,
                        COM_Prenom = item.COM_Prenom,
                        COM_Statut = item.COM_Statut,
                        COM_Ville = item.COM_Ville,
                        Client = cli,
                        
                    });
                }
                return ListCommande;
            }
        }
        public static void AjouterCommande(CommandesModel Commande, string username)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                ClientsModel cli = BLClients.FindClientByName(username);
                dbContext.Commande.Add(new Commande
                {
                    COM_Adresse = Commande.COM_Adresse,
                    COM_Civilite = Commande.COM_Civilite,
                    COM_CLI_Id = cli.CLI_Id,
                    COM_CodePostal = Commande.COM_CodePostal,
                    COM_Date = DateTime.Now,
                    COM_DateLivraison = Commande.COM_DateLivraison,
                    COM_Id = Commande.COM_Id,
                    COM_Nom = Commande.COM_Nom,
                    COM_Prenom = Commande.COM_Prenom,
                    COM_Statut = Commande.COM_Statut,
                    COM_Ville = Commande.COM_Ville,

                });
                dbContext.SaveChanges();
            }

        }
        public static CommandesModel FindCommandeById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Commande Commande = dbContext.Commande.Find(id);
                var cli = BLClients.GetAllData().SingleOrDefault(x => x.CLI_Id == Commande.COM_CLI_Id);
                CommandesModel commande = (new CommandesModel
                {
                    COM_Adresse = Commande.COM_Adresse,
                    COM_Civilite = Commande.COM_Civilite,
                    COM_CLI_Id = Commande.COM_CLI_Id,
                    COM_CodePostal = Commande.COM_CodePostal,
                    COM_Date = Commande.COM_Date,
                    COM_DateLivraison = Commande.COM_DateLivraison,
                    COM_Id = Commande.COM_Id,
                    COM_Nom = Commande.COM_Nom,
                    COM_Prenom = Commande.COM_Prenom,
                    COM_Statut = Commande.COM_Statut,
                    COM_Ville = Commande.COM_Ville,
                    Client = cli,

                });
                return commande;

            }

        }
        public static void DeleteById(int? id)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Commande Commande = dbContext.Commande.Find(id);
                dbContext.Commande.Remove(Commande);
                dbContext.SaveChanges();

            }

        }
        public static CommandesModel Edit(CommandesModel Commande)
        {
            using (WebShopEntities dbContext = new WebShopEntities())
            {
                Commande commande = new Commande
                {
                    COM_Adresse = Commande.COM_Adresse,
                    COM_Civilite = Commande.COM_Civilite,
                    COM_CLI_Id = Commande.COM_CLI_Id,
                    COM_CodePostal = Commande.COM_CodePostal,
                    COM_Date = DateTime.Now,
                    COM_DateLivraison = Commande.COM_DateLivraison,
                    COM_Id = Commande.COM_Id,
                    COM_Nom = Commande.COM_Nom,
                    COM_Prenom = Commande.COM_Prenom,
                    COM_Statut = Commande.COM_Statut,
                    COM_Ville = Commande.COM_Ville
                };
                dbContext.Entry(commande).State = System.Data.Entity.EntityState.Modified;

                dbContext.SaveChanges();
                return Commande;
            }

        }
    }
}