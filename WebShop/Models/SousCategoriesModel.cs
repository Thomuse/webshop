﻿using WebShop.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShop.Models
{
	public class SousCategoriesModel
	{
		[Display(Name = "ID")]
		public int SCAT_Id { get; set; }
		[Display(Name = "Catégorie")]
		public int SCAT_CAT_Id { get; set; }
		[Display(Name = "Sous-Catégorie")]
		public string SCAT_Libelle { get; set; }

		public virtual ICollection<ArticlesModel> Article { get; set; }
		public virtual CategoriesModel Categorie { get; set; }

		
	}
}