﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebShop.DAL;

namespace WebShop.Models
{
    public class DetailsCommandeModel
    {
       
        public int DCOM_Id { get; set; }

        public int DCOM_COM_Id { get; set; }

        public int DCOM_ART_Id { get; set; }
        [Display (Name = "Quantité")]
        public int DCOM_Quantite { get; set; }
        [Display (Name = "Prix à l'unité")]
        public decimal DCOM_PrixUnitaire { get; set; }

        public virtual ArticlesModel Article { get; set; }

        public virtual CommandesModel Commande { get; set; }

       
    }
}