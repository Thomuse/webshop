﻿using WebShop.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShop.Models
{
    public class ArticlesModel
    {
        [Required]
        public int ART_Id { get; set; }
        [Required]
        [Display (Name= "Sous-Catégorie")]
        public int ART_SCAT_Id { get; set; }
        [Required]
        [Display(Name = "Article")]        
        public string ART_Libelle { get; set; }
        [Required]
        [Display (Name = "Description")]
        public string ART_Description { get; set; }
        [Required]
        [Display (Name = "Prix")]
        public decimal ART_Prix { get; set; }
        public string Image { get; set; }
        
        public HttpPostedFileBase uploadimage { get; set; }
        [Display (Name = "Disponible")]
        public int ART_Stock { get; set; }
        [Display (Name = "Sous-Catégorie")]
        public virtual SousCategoriesModel SousCategorie { get; set; }
        public virtual ICollection<DetailsCommandeModel> DetailCommande { get; set; }

     
       
    }

}