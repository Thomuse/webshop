﻿using WebShop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    public class CommandesModel
    {
        public int COM_Id { get; set; }
        [Display (Name = "Catégorie")]
        public int COM_CLI_Id { get; set; }
        [Required]
        [Display (Name = "Date de Commande")]
        //[DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime COM_Date { get; set; }
        [Required]
        [Display (Name = "Statut de la Commande")]
        public string COM_Statut { get; set; }
        //[Required]
        //[DataType(DataType.Date)]
        [Display(Name = "Date de Livraison")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime COM_DateLivraison { get; set; }
        [Required]
        [Display (Name = "Nom")]
        public string COM_Nom { get; set; }
        [Required]
        [Display (Name = "Prénom")]
        public string COM_Prenom { get; set; }
        [Required]
        [Display (Name = "Civilité")]
        public string COM_Civilite { get; set; }
        [Required]
        [Display (Name = "Adresse")]
        public string COM_Adresse { get; set; }
        [Required]
        [Display (Name = "Code Postal")]
        public string COM_CodePostal { get; set; }
        [Required]
        [Display (Name = "Ville")]
        public string COM_Ville { get; set; }

        //public decimal? Total { get; set; }

        public virtual ClientsModel Client { get; set; }
        public virtual ICollection<DetailsCommandeModel> DetailCommande { get; set; }

      
        }
    }
