﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebShop.DAL;

namespace WebShop.Models
{
    public class PanierModel
    {
        //public int PanierId { get; set; }

        //[Required]
        //[StringLength(50)]
        //public string CartId { get; set; }

        //public int ArticleId { get; set; }

        //public int Count { get; set; }

        //public DateTime? DateCreate { get; set; }

        public List<Panier> CartItems { get; set; }
        public decimal CartTotal { get; set; }

        public virtual ArticlesModel Article { get; set; }
    }
}