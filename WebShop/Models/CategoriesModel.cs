﻿using WebShop.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShop.Models
{
    public class CategoriesModel
    {
        
    
        public int CAT_Id { get; set; }
        [Required]
        [Display (Name = "Libellé")]
        public string CAT_Libelle { get; set; }
        [Display (Name = "Sous-Catégorie")]
        public virtual ICollection<SousCategoriesModel> SousCategorie { get; set; }

       

    }

}