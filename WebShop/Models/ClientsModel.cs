﻿using WebShop.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebShop.Models
{
    public class ClientsModel
    {
        [Required]
        [Display(Name = "ID")]
        public int CLI_Id { get; set; }
        [Required]
        [Display(Name = "Nom")]
        public string CLI_Nom { get; set; }
        [Required]
        [Display(Name = "Prénom")]
        public string CLI_Prenom { get; set; }
        [Required]
        [Display(Name = "Civilité")]
        public string CLI_Civilite { get; set; }
        [Required]
        [Display(Name = "E-mail")]
        public string CLI_Email { get; set; }
        [Required]
        [Display(Name = "Adresse")]
        public string CLI_Adresse { get; set; }
        [Required]
        [Display(Name = "Code Postal")]
        public string CLI_CodePostal { get; set; }
        [Required]
        [Display(Name = "Ville")]
        public string CLI_Ville { get; set; }
        [Required]
        [Display(Name = "Telephone")]
        public string CLI_Telephone { get; set; }

        public virtual ICollection<CommandesModel> Commande { get; set; }


       
      
       
    }
}